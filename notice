#!/usr/bin/env bash

# "Notice": a simple motd-like script that uses as single file (which you'd probably want to sync across systems) to produce log-in notices for any of the systems you use
#The idea is that you can edit the one file on your desktop, for example, to remind you of something on your laptop

# File format example:
#group:work:Excelsior
#group:non-work:rogueone,bespin,falcon,frigate,intrepid
#rogueone:catch up on webcomics
#rogueone:learn GnucCash
#rogueone,falcon,frigate:work on mdis script - prompt
#rogueone,falcon,frigate:modify notice to have groups and/or negations
#falcon:move powerstat.txt under backups/

set -u   #treat undeclared variables as errors

function helpMode() {
    echo "$progname usage:"
    echo "  $progname -h|--help     print this text"
    echo "  $progname -d|--display  display the notice text for this machine"
    echo "  $progname -e|--edit     edit the notice file with your \$EDITOR"
}

function displayMode() {
    function filterComments() {
        #Filter comments out
        grep -v "^[[:space:]]*#" "${@:-}"
    }

    #Exit quietly if the file isn't present, or there's a "private mode" flag file present (useful for screenshots/screencasts/asciinema)
    [[ -e ~/.privatemode || ! -e $noticeFile ]] && exit 5

    groups=$(filterComments "$noticeFile" |grep -E "^group:.*[:,]$hostname(,|$)" |cut -f2 -d:)
    notices=$(filterComments "$noticeFile" |grep -E "^[^:]*(^|,)(all|$hostname)[:,]" |cut -f2- -d:)
    for group in $groups; do
        notice="$(filterComments "$noticeFile" |grep -E "^$group:" |cut -f2- -d:)"
        [[ $notice ]] && notices+="${nl}${notice}"
    done
    notices="${notices#$nl}"    #remove any leading newlines
    if [[ $notices ]]
    then
        echo "$notices" |sed "s/^/Notice: /" |wordwrap
    fi
}

function editMode() {
    ${EDITOR:-${VISUAL:-vi}} "$noticeFile"
}

function wordwrap { fold -sw $(tput cols); }

noticeFile="$HOME/backups/notice.txt"
progname="$(basename $0)"
hostname="$(hostname -s)"
nl=$'\n'    #newline


case "${1:-}" in
    *-d*)   displayMode;;
    *-e*)   editMode;;
    *-h*|*) helpMode;;
esac
