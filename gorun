#!/usr/bin/env bash

#This is a script that builds and manages go executables for very simple "scripts"
#The idea is that you can distribute simple one-file go source code with a
# "#!/usr/bin/env gorun" shebang and have gorun manage actually compiling the
# source and running the executable, so you don't have to worry about managing
# different executables for different architectures and operating systems

#tl;dr, this script lets you use golang as a compiled scripting language

set -u   #treat undeclared variables as errors

function warn() { echo "$*" >&2; }
function die()  { warn "$*"; exit 1; }

progname="$(basename $0)"
source="${1:-}"
sourceBase="$(basename "$source" |sed 's/\.go$//')"

[[ $source ]] || die "Please specify a file to \"go run.\" You may use this script as the shebang"

[[ -e $source ]] && [[ -r $source ]] || die "Unable to access source file $source"
temproot="${TMPDIR:-/tmp}"
tempdir="$temproot/${progname}.$(whoami)"
mkdir -p "$tempdir" || die "failed to create tempdir $tempdir"
chmod 700 "$tempdir"

hash="$(md5sum "$source" |awk '{print $1}')"
tempSource="$tempdir/$sourceBase.go"
executable="$tempdir/$sourceBase.$hash"

if ! [[ -e $executable && -x $executable ]]; then
    grep -v "^#!/.*gorun" "$source" > "$tempSource"
    go build -o "$executable" "$tempSource" || die "go build of $source failed; aborting"
    rm "$tempSource"
fi

exec "$executable"
