#!/bin/bash

# I used this script for a while to kick off the `motion` command as a security camera whenever my computer was locked

#configuration
shopt -s expand_aliases		#needed for the aliases to work
lockprog=i3lock
alias motion='motion -c ~/.config/motion.conf'
alias pgrep='pgrep -x'
delay=10	#seconds
videodir=$(grep "^target_dir " $HOME/.config/motion.conf |tail -1 |cut -f2- -d' ')
if [[ $1 == "--debug" ]]
then
	debug=true
	shift
else
	debug=false
fi


function warn() {
	echo "$*" >&2
}

function correctJpegExtensions() {
	$debug && warn $(date) "correcting .jpg extensions to .jpeg:"
	for x in *.[jJ][pP][gG]
	do
		y=$(echo "$x" |sed 's/\.[jJ][pP][gG]$/.jpeg/')
		$debug && warn "	correcting $x to $y"
		mv "$x" "$y"
	done
	$debug && warn			#extra line
}

while :		#run forever
do
	if pgrep $lockprog >/dev/null				#If screen is locked:
	then
		$debug && warn $(date) "screen is locked, waiting 30 seconds..."
		sleep 30	#wait 30 seconds to make sure room is clear

		#start motion if it isn't already running
		if pgrep motion >/dev/null
		then
			$debug && warn $(date) "motion is already running"
		else
			$debug && warn $(date) "starting motion"
			motion
		fi

		#wait until the lock is gone, then kill motion
		pwait $lockprog
		$debug && warn $(date) "screen unlocked; stopping motion"
		killall motion
		$debug && warn $(date) "renaming .jpgs to .jpegs"
		if [[ -d $videodir ]]
		then
			pushd "$videodir"
			sync
			sleep 3
			correctJpegExtensions
			popd
		fi
	else										#If screen is not locked:
		#stop motion if it's running
		if pgrep motion >/dev/null
		then
			$debug && warn $(date) "screen not locked; stopping motion"
			killall motion
		fi
	fi

	$debug && warn "sleeping $delay seconds..." && warn
	sleep $delay
done
