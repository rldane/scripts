# scripts
todo: update this README

Some of my scripts that I wrote, many of which I use frequently.

I've tried to add some explanatory comments to the beginning of each one

Any comments you see that look like "## needsterm ##" at the beginning of a file are tags that I sometimes use

## Contents:

* LICENSE - MIT License
* README.md - This file

* acpi_upower - Linux script to scrape upower and produce acpi-like output. also projects total battery or charging time based on remaining and percentage
* addscript - Creates a new script in my ~/bin or ~/bin/scripts directory, populates it with a shebang and shopt, makes it executable, and opens it in the $EDITOR
* aptsearch - "apt search" is a little slow, and produces visually bloated output (3 lines per entry). This script calls "apt-cache search" for a slightly faster search, but also caches a list of packages from "dpkg -l" (updating if the list is over 4 hours old), and uses that to print an indication if a package in the search results is installed (like "apt search"). Since it's calling "apt-cache search," the output is a lot leaner (only one line per entry). Requires the "fileage" script in this repo
* backlight - Control backlight in Linux and OpenBSD, useful when bound to keys like Ctrl-Super-+/-
* battnotify - Script gets battery status in Linux and OpenBSD and sends a notification via notify-send
* bibleweb - When provided a book and chapter (and optional translation) as parameters, it fetches the text from biblehub.com, does some sed processing to remove extraneous footnotes and unwanted characters, and either displays it in the terminal or using enscript and zathura
* blogme - A script to simplify writing a pelican-style markdown blog post. Automatically fills out the header with date and title (based on first line entered)
* blogwordcount - A script that queries a [Pelican](https://getpelican.com/)-generated blog and shows a word count (and lines, and characters, too) for each blog post. Only tested with one theme, may not work on your Pelican site
* bt - a TUI-ish (menu-driven) front-end to bluetoothctl for simple commands like connect and disconnect
* calc - bc-based calculator for dmenu
* centertext - Takes text input from `$*` (ARGV) or STDIN and centers it, filling the left and right margins with spaces. Uses tput to get current terminal columns
* clipboardtoprimarysel - Move contents of clipboard to primary selection buffer
* clipclear - Clear the primary and secondary selection buffers, and clipboard
* clipdisp - Clipboard dispatcher: runs xclip or wl-copy/paste depending on your environment
* clipwatch - Watch the clipboard for changes and print them
* cpubar - Prints a CPU utilization bar horizontally down the terminal
* cpugov - Get status of or control cpu governors (similar to powerprofilesctl, but dead-simple)
* cputoggle - provides an easy interface for turning individual CPU cores on and off. Requires that the /sys/devices/system/cpu/cpu\*/online files are writeable to the user. Optionally depends on "lscpu" for a pretty status display
* cwmstatus - Takes the output of i3bar and adds the current cwm window group number. Also calls xterm to place the bar on the bottom of the screen (will likely need tweaking for your monitor setup). Depends on obsdstatus, which in turn depends on i3status (comes with the i3/i3-gaps package)
* darkmode - This is an experimental script to set a dark mode for gtk & qt apps. It is based on scripts found in the dark-mode.d directory of https://gitlab.com/WhyNotHugo/darkman.git
* datestamp - a simple script to print a standard formatted datestamp or put it in the system clipboard
* daylog - A little one-line-per day journal program. Looks for and appends to a file called daylog.txt in ~/Documents/Journal/
* diceroll - Prints a percentage (a roll of 2D10 or 1D100), using /dev/urandom as the PRNG
* displaywindowtitle - uses zenity to display the current window title. Useful for window managers that don't display a title bar, or for windows that may have a very long title
* dot-bash_functions-sample - A sample of my ~/.bash_functions file. Some functions in this file are needed by two of these scripts: duh (mountpoint() (but only for OpenBSD) and pkg_size (simplify())
* dotconfig-i3status-config-sample - A sample of my i3status config. obsdstatus and cwmstatus may be dependent on some particulars of how i3status is configured/formatted
* dpilove - A simple script to calculate the ppi of a screen. Based loosely on the https://dpi.lv website. Created because I found that site too slow to load
* duck - A command-line interface to DuckDuckGo, which can call a variety of graphical and terminal web browsers depending on configuration and command-line options
* duh - Like running du -sh * |sort -h, but including hidden files and excluding mountpoints. Requires the mountpoint() function in my bash_functions sample file if on BSD (calls the mountpoint command in Linux). Note: the mountpoint() function is somewhat half-baked, but is an initial best effort at reproducing the functionality
* emoji-picker - My fork of https://github.com/end-4/fuzzel-emoji, works in X11, Wayland, and terminal
* fcal - The calendar filter - takes a set of days as parameters and filters the output of cal/ncal/gcal to show only those days on the calendar
* fclist-to-csv - This script runs fc-list and dumps every possible value using the tags found in fontconfig.h Depends on XOrg (obviously) and fc-list
* fileage - Reports the age of a file in hours. Needed by updateuserdb
* filefind - Finds a file based on regex and uses dmenu as a picker to open it. Dependent on updateuserdb
* fileparentdirfind - Finds a file's parent directory based on regex and uses dmenu as a picker to open it. Dependent on updateuserdb
* filesizebreakdown - Find all the files in the current directory and gives a breakdown of storage used by each file type (by extension)
* findmenu - A dmenu/bemenu interface for triggering the filefind/folderfind/fileparentdirfind scripts
* folderfind - Finds a folder based on regex and uses dmenu as a picker to open it. Dependent on updateuserdb 
* fontpicker - lets the user pick a console font and uses setfont to change the console/fb font. Only tested on Debian-based and Arch-based Linux distros
* formatphonenumber - Simply take the clipboard, detect a (U.S.) phone number, and format it nicely, e.g., (123) 456-7890
* gorun - a shebang handler for running go source code like scripts. Caches compiled output for faster execution using an md5 hash of the source code
* gping - Like cpubar, shows ping times in a graphical way going down the terminal
* ipscan - uses ping and netcat to scan the subnet for devices
* kjvfilter - a sed script that attempts to filter the "Authorized Version" / "King James Bible" language into something approaching modern English.
* manfind - implements `man -K` (full text search of man db) on systems lacking that function, such as the BSDs
* mdis - menu dispatcher, detects X11 or wayland and tries to find a suitable menu utility, like dmenu, bemenu, rofi, wofi, or fuzzel
* mkcachedir - A script I created in OpenBSD to create directories in /tmp for symlinks pointing to /tmp from ~/.cache (using RAMDisk as cache for performance).
* motionwatcher - A script I wrote to start the 'motion' security camera app whenever the screen was locked. Hasn't been used in a while
* mousetoggle - A simple script to toggle the mouse pointer on or off using xinput (X11 only). Written for my current OpenBSD laptop
* naturalscrolling - Uses xinput to enable "natural" scrolling direction for trackpads
* new_workspace - For i3wm (X11): Figure out which is the next empty workspace and switch to it
* nmapscan - Calls nmap to scan the subnet
* notice - a user-level motd-like utility meant to use a single text file synced between all your systems
* obsdstatus - calls i3status and adds the missing memory usage info for OpenBSD
* patchlog - Scans through the /var/syspatch directory in OpenBSD and lists the installed patch names, files, and comments
* pinglog - Simple script to ping an ip address on the internet every minute and keep a log of internet uptime
* pkgbins - Searches the file list for the given package names and returns the files in various bin directories (just greps for 'bin'). Works in FreeBSD, OpenBSD and Debian, RPM, and Arch-based Linux distros
* pkg_size - OpenBSD: takes a list of packages from the command line and calls pkg_add -mis to determine the amount the filesystems would grow by if the package (and all its uninstalled depenencies) were installed. Calls the simplify function from my .bash_functions to convert any number of bytes to the largest SI unit equivalent (e.g., KiB, MiB, GiB)
* pkgsizes - Shows all installed packages sorted by increasing size. Works on Debian, rpm, and Arch-based Linux distros and OpenBSD
* powertrack - Linux-only: calls acpi_upower (another script here) with some parameters every 60 seconds and logs the output to ~/powerstat.txt. Useful for observing trends in power consumption and battery life in laptops
* primaryseltoclipboard - Copies the primary selection buffer to the system clipboard
* processcheck - Given a list of programs as args, it checks to see if they are running and prints an error if they aren't. I use this in my .bashrc to make sure that things like syncthing and redshift are running
* psgrep - like pgrep, but shows the actual ps output, optionally summing up the memory used by all listed processes
* say - Tries to find a speech synthesis program to speak the words given by STDIN or arguments
* scrunch - lossy PNG compressor. Not as good as manually doing it in GIMP. Depends on imagemagick's `convert`
* searchall - searches all package managers that I'm familiar with: apt, zypper, pacman, OpenBSD pkg_locate or https://openports.pl, yay (AUR), flatpak, snap, and brew.
* showclip - Shows the contents of the clipboard both in X11 and Wayland
* smartcd - relies upon my ulocate family of scripts and fzf. You give it a search path, fzf lets you pick a directory, and it goes to it
* smartman - Calls man, falls back to using tldr if the manpage isn't found, calls back to running $command --help if tldr doesn't find anything
* sndionotify - watches sndioctl -m (OpenBSD) and sends a notification when the system volume changes
* stsizes - Shows the size of syncthing folders, useful for deciding which folders to sync on a new machine with limited space
* suspend - Simple script that syncs the buffers a couple times and calls the command to suspend the machine. For Linux, OpenBSD, and FreeBSD
* swayrot - rotate a swaywm screen
* syncthing-quick-status - my minor fork of https://github.com/serl/syncthing-quick-status.git
* system_updates_checker - attempts to be a very fast script to check the recency of system updates and remind the user if updates are needed. I run it in my .bashrc on every login
* temperaturechart - prints a temperature char down the terminal like cpubar and gping. Relies on vcgencmd (Raspberry Pi) or sensors (general Linux)
* temps - Attempts to report current system temperature in OpenBSD and Linux.
* termbar - My version of https://github.com/vetelko/termbar/blob/master/bars/gonzalo/termbar. I no longer use it in favor of cwmstatus
* terminaldir.desktop - the desktop file for the terminaldir script
* terminaldir - Spawns a terminal at a working directory given as an argument. useful when paired with terminaldir.desktop and used as the default program for opening directories (instead of opening a file manager)
* termsize - reports the size of the terminal once or until `q` is pressed. Created because some terminals don't display the size of the terminal (in columns and rows) as they are being resized
* textrot10+3 - A silly rot13 variant the rotates consonants and vowels separately, producing vaguely pronouncable output
* textrot13 - A very basic rot13 program
* todo-txt/timetracker - A front-end for the timetracker plugin to todo.sh that adds a lot of features
* todo-txt/todo - A front-end for todo.sh/todo-txt which adds a lot of features
* todo-txt/tstat - A quick status display for todo.sh
* tootcounter - Uses `toot whoami` to keep a daily counter of how many toots you toot on Mastodon
* tootme - My front-end for the toot-editor mode. Gracefully handles failures and toot-too-long conditions
* tootuser - interactively look up a fediverse user using toot. Pick one with a menu (depends on mdis), and copy to the clipboard (depends on clipdisp)
* trackpad - Enable and disable the trackpad. uses xinput
* tupdate - A script I created to update the `tut` Mastodon TUI client using `go install` and git. Also installs the manpages for tut
* tut-all-users - A simple script to get all registered userids from the `accounts.toml` file in ~/.config/tut/ and call `tut -u {accounts}` so that you can easily flip between accounts in [tut](https://github.com/RasmusLindroth/tut)
* ulocate - my own `locate` script, just for files in my home directory. used by filefind/folderfind/fileparendirfind and calls updateuserdb
* updateall - Updates all package managers I'm familiar with. Runs pkcon for KDE Neon, apt for debian-based distros, pacman for arch-based distros, zypper for OpenSuSE, yay for the AUR, syspatch and pkg_add on OpenBSD, checks freebsd-update (but doesn't update) and runs pkg upgrade on FreeBSD, and secondary sources flatpak, snap, and homebrew
* updateuserdb - the updatedb equivalent of my ulocate command
* urlsanitize - Attempts to sanitize links to icky websites like youtube to more human-friendly alternative front-ends like Invidious
* volume - Changes the system volume in Linux (using amixer), OpenBSD, and FreeBSD. Useful with a hotkey daemon, or a hotkey service within a tiling window manager
* vrms-pacman - a "Virtual Richard M. Stallman" program (https://packages.debian.org/bullseye/vrms) for pacman. Usually fails miserably because far too many Arch packages lack the license info
* windowlist - list windows in your swaywm session
* wofi-calc - This is my fork of https://github.com/Zeioth/wofi-calc I've cleaned it up a good bit and hopefully made it more readable.
* winresize - Uses mdis to pop up a message asking what resolution to resize the window to, and then runs xdotool to select the window and then resize it. Useful when you want to resize a window to a precise value, such as setting your browser window to a very common full-screen-equivalent size to make fingerprinting more difficult.
* workspace-name - Uses xprop to get the name of the current workspace / virtual desktop in both X11 and Wayland. Falls back to specifying the desktop number if no name is found.
