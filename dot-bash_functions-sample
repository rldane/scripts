#
# ~/.bash_functions
#

# vim: filetype=bash

[[ $BASH_FUNCTIONS ]] || BASH_FUNCTIONS=true
typeset -r BASH_FUNCTIONS

#important functions:
function is_available { type "$@" &>/dev/null; }
function is_installed { type "$@" &>/dev/null; }
function is_inst      { type "$@" &>/dev/null; }
function installed    { type "$@" &>/dev/null; }
function inst         { type "$@" &>/dev/null; }


function warn {
    echo -e "$*" >&2
}

function die {
    warn "$*"
    exit 1
}

function fdie {	#die() for functions (uses return instead of exit)
    warn "$*"
    return 1
}
function version {
    [ "$1" ] || return 1
    pacman -Q $(pacman -Qo $(which $1) |sed 's/^.*owned by //' |cut -f1 -d " ")
}

function start {
    eval "$@" &>/dev/null & disown
}

function pagedhelp {    #(for bash)
    \help "$@" |less -iX
}
alias help=pagedhelp


#Neither pathclean functions are necessary any longer. Left in, just in case
function pathclean {
    local newpath=
    for x in $(echo "$PATH" |sed "s/ /--space--/g" |tr ":" " ")
    do
        echo "$newpath" |grep -Eq "(^|:)$x(:|$)" || newpath="$newpath:$x"
    done
    PATH=$(echo "$newpath" |sed "s/^://; s/--space--/ /g")
}

function manpathclean {
    newmanpath=
    for x in $(echo "$MANPATH" |sed "s/ /--space--/g" |tr ":" " ")
    do
        echo "$newmanpath" |grep -Eq "(^|:)$x(:|$)" || newmanpath="$newmanpath:$x"
    done
    MANPATH=$(echo "$newmanpath" |sed "s/^://; s/--space--/ /g")
}

function dpkg-list-installed {
    [ "$1" ] || return 1

    dpkg -l "$*" |awk '$1~/^i/ {print $2}' |sed 's/:[^:]*//'
}

function wordwrap {
    local fmt fmtopt cols
    case $(uname) in
        Linux)      fmt=fmt;;
        OpenBSD)    fmt=gfmt;;
    esac
    case "$1" in
        --no-indent|-n)	fmtopt=
            shift;;
        *)		fmtopt="-t";;
    esac
    cols=$(tput cols)
    if [ "$1" ]
    then
        $fmt -$cols $fmtopt <"$1"
    else
        $fmt -$cols $fmtopt
    fi
}

function swapinfo {
    for file in /proc/*/status
    do
        awk '/VmSwap|Name/ {printf $2 " " $3} END {print ""}' $file
    done | sort -k 2 -n
}

function mcd {
    #mkdir & cd
    if [ "$1" ]
    then
        mkdir "$1"
        cd "$1"
    fi
}

function sysinfo {
    local command='inxi -Fxxxza --no-host'
    echo "\$ $command"
    $command
}

function right_justify {
    local text text_length pad
    text="$*"
    text_length=$(echo -n "$text" |wc -c)
    ((pad=$COLUMNS-$text_length))
    printf "%${pad}s%s\n" "" "$text"
}

function capitalize_word {
    local first_letter=$(echo "$*" |cut -c1)
    local rest=$(echo "$*" |cut -c2-)
    echo -n "$first_letter" |tr a-z A-Z
    echo "$rest"
}

function chr {
    [ "$1" -lt 256 ] || return 1
    printf "\\$(printf '%03o' "$1")"
}

function ord {
    LC_CTYPE=C printf '%d' "'$1"
}

function print_de_banner {
    #running (desktop) on (windowing system) banner
    local de_banner_text ppname
    de_banner_text=
    ppname=$(basename $PPNAME)
    if [ "$XDG_SESSION_DESKTOP" -a "$XDG_SESSION_TYPE" ]
    then
        de_banner_text="(Running $ppname on $(capitalize_word $XDG_SESSION_DESKTOP)-$(capitalize_word               $XDG_SESSION_TYPE))"
    elif [ "$XDG_SESSION_DESKTOP" ]
    then
        de_banner_text="(Running $ppname on $(capitalize_word $XDG_SESSION_DESKTOP))"
    elif [ "$XDG_SESSION_TYPE" ]
    then
        de_banner_text="(Running $ppname on $(capitalize_word $XDG_SESSION_TYPE))"
    fi

    if [ "$de_banner_text" ]
    then
        right_justify $de_banner_text
    fi
}

function winsize {
    xprop |grep REGION |awk -F, '{print $3"x"$4}' |tr -d " "
}

function foxmem {
    ps aux |awk '$11~/(firefox|purebrowser|icecat|palemoon)/ {sum+=$6} END{ print "Firefox is currently using "sum/1024" MiB of resident RAM"}'
}
function firefoxmem {	foxmem; }

function falkonmem {
    local mem
    mem=$(ps aux |awk '$11~/(falkon|QtWebEngineProcess)/ {sum=sum+$6} END{ sum=sum/1024; print sum }')
    [ -z "$mem" ] && mem=0

    echo "Falkon is currently using $mem MiB of resident RAM"
}

function 2stageprogmem {    #used to calculate memory usage of programs that spawn a lot of child processes, like web browsers
    local prog level1data ppidre level2data
    [[ $1 ]] && prog=$1 || return 148

    level1data=$(ps axo pid,rss,command |grep -E "[[:space:]][0-9]+[[:space:]]$prog([[:space:]]|$)" |tr -s ' ' |sed -E 's/^[[:space:]]+//')
    [ "$level1data" ] || return 1

    ppidre=$(echo "$level1data" |cut -f1 -d' ' |tr "\n" "|" |sed 's/|$//')
    ppidre="^[[:space:]]*($ppidre)[[:space:]]"

    level2data=$(ps axo ppid,rss,command |grep -E "$ppidre")

    #Total the memory usage:
    echo "$level1data" |awk '{sum+=$2/1024}; END {print sum" MiB used by '$prog' processes"}'
    echo "$level2data" |awk '{sum+=$2/1024}; END {print sum" MiB used by '$prog' subprocesses"}'
    printf "%s\n%s\n" "$level1data" "$level2data" |awk '{sum+=$2/1024}; END {print sum" MiB total used by '$prog'"}'
}

surfmem()   { 2stageprogmem surf;    }
luakitmem() { 2stageprogmem luakit;  }
chromemem() { 2stageprogmem chrome:; }

function evolutionmem {
    ps axo user,pid,ppid,%cpu,%mem,vsz,rss,tty,stat,start,time,command |grep $(pgrep evolution |sed -E 's/(^|$)/\\b/g; s/^/-e /') |awk 'sum+=$7; END {print sum}' |awk '{sum+=$7} END{ print "Evolution is currently using "sum/1024" MiB of resident RAM"}'
}
function evomem {		evolutionmem; }

function ramhogs {
    COLUMNS=999 ps aux |sort -nsk 6 |awk '$6/=1024' |cut -f6,11- -d' ' |cut -c 1-$COLUMNS
    for x in firefox falkon evolution surf chrome luakit
    do
        if pgrep $x &>/dev/null 
        then
            echo
            ${x}mem
        fi
    done
}

function cheat {
    #Looks up command in https://cheat.sh for a quick guide
    # \less uses less ignoring any aliases
    curl -s "https://cheat.sh/$*" |\less -riX
}

function prominentcolor {
    for file in "$@"
    do
        printf "median: "
        [ "$file" ] && printf "%-30s\t#%02x%02x%02x\n" "$file" $(identify -verbose "$file" |grep median: |head -3 |awk '{print $2}')
        printf "mean:   "
        [ "$file" ] && printf "%-30s\t#%02x%02x%02x\n" "$file" $(identify -verbose "$file" |grep mean: |head -3 |awk '{print $2}' |cut -f1 -d. |tr "\n" " ")
    done
}

function filename_from_ls-l {
sed -E 's/^[-a-z]{10}[[:space:]]+[0-9]+[[:space:]]+([a-zA-Z0-9]+[[:space:]]+){2}[0-9]+[[:space:]]+[A-Z][a-z][a-z][[:space:]]+[0-9]+[[:space:]]+[0-9]{2}:?[0-9]{2}[[:space:]]+//'
}

function stls {
    #Syncthing-aware ls
    local line file

    ls -l "$@" |while read line
do
    file=$(echo "$line" |filename_from_ls-l)
    [[ -d $file/.stfolder ]] && echo -n "st " || echo -n "   "
    echo "$line"
done
}

function isalias {
    type "$@" 2>&1 |grep -q alias
}

function chomp {
    local str="$*"
    echo "${str%$'\n'}"
}

function backlightpause {
    #Records the current baclight value, sets it to 0, pauses for keyboard
    # input, then sets it back to the original value
    local l=$(light |tr -dc 0-9.)
    light -S 0
    read -sp "-- PAUSE -- (press a key)" -n1
    light -S $l
    echo
}

function tldr_with_fallback {
    if ! \tldr "$@" 
    then
        #use cheat.sh
        local cheatHelp=$(curl -s "https://cheat.sh/$*")
        if echo "$cheatHelp" |grep -qi "Unknown topic"
        then
            echo "cheat.sh responded \"Unknown topic.\" Falling back to man"
            man "$@"
        else
            echo "$cheatHelp" |less -riX
        fi
    fi
}

#Functions to modify $IFS variable:
function ifsn {	#read: function: IFS=$'\n'
    IFS=$'\n'
}
function ifsdefault {
    IFS=$' \t\n'
}	

function jpeg2mjpeg {
    [[ $# = 2 ]] || fdie "Usage:\n  jpeg2mpeg file|glob output_file"
    ffmpeg -framerate 30 -pattern_type glob -i "$1" -codec copy "$2"
    #honestly, it just does the exact same thing as 'cat *.jpeg > vid.mjpeg'
}

function linuxnotes {
    local file editor
    file="$HOME/Documents/Notes/linuxnotes.txt"
    editor=vim
    [[ $1 =~ ^-[xX]$ ]] && editor=gvim

    inst $editor || editor="$EDITOR"
    inst $editor || fdie "Couldn't find the editor: $editor"

    if [[ $editor == gvim ]]
    then
        $editor $file &>/dev/null & disown
    else
        $editor $file
    fi
}

function yman {
    #Use Gnome yelp to display man pages
    local section
    installed yelp || ( warn "yelp isn't installed"; return 1 )
    local section=
    if [[ $1 =~ ^[0-9]+[a-z]*$ && $2 ]]
    then
        section=$1
        shift
    fi
    if [[ $section ]]
    then
        yelp "man:$1($section)"	&>/dev/null & disown
    else
        yelp "man:$1"			&>/dev/null & disown
    fi
}

function sdf {    #s[mart] df
    df -Th |awk '$2 !~ /tmpfs/'
}

function showclip {
    for selection in "Primary Selection" "Secondary Selection" "Clipboard"
    do
        local output=$(xclip -o -selection $selection 2>&1)
        local outputlines=$(echo "$output" |wc -l)
        local hr=" - - - - - - - - - - - - - - - - - - - "
        #echo "DEBUG: $outputlines"
        #echo "DEBUG: selection=\"$selection\", output=\"$output\", outputlines=\"$outputlines\""
        if (( outputlines > 1 )); then
            echo -e "\n\t$selection:\n$hr\n$output\n$hr\n"
        else
            echo -e "$selection: «$output»"
        fi
    done
}

function ls_dirs_first {
    local dirs files
    IFS=$'\n'
    files=
    dirs=
    for x in $(ls -F)
    do
        [[ $x == */ ]] && dirs="$dirs:$x" || files="$files:$x"
    done
    echo "${dirs}${files}" |tr : "\n" |grep -v "^[[:space:]]*$" |column
}

function ls-l_dirs_first {
    local output=$(\ls -l)
    echo "$output" |grep "^total"
    echo "$output" |grep "^[[:space:]]"
    echo "$output" |grep "^d"
    echo "$output" |grep "^[-bclps]"
}

function xclipo {
    xclip -o -selection clipboard
    [[ $1 == -* ]] && echo  #add a newline if any kind of parameter was added (-e, --echo, -n, --newline, whatever ;)
}

if inst pkg_info
then
    function pkginfo {
        local opts first
        opts=
        while [[ $1 =~ ^- ]]; do
            opts="$opts $1"
            shift
        done
        first=true
        for x in "$@"; do
            $first || echo
            pkg_info $opts "$x"
            first=false
        done
    }
fi

if ! inst killall
then
    function killall {
        echo "> pkill $@"
        sleep 0.2
        pkill "$@" && echo "(success)" || echo "(failed: return code $?)"
    }
fi

if ! inst free && inst vmstat
then
    function free {
        #What we're aiming for:
        #               total        used        free      shared  buff/cache   available
        #Mem:           3.7Gi       907Mi       261Mi        62Mi       2.6Gi       2.6Gi
        #Swap:          2.0Gi        67Mi       1.9Gi
        vmstat
        inst swapctl && swapctl -l
    }
fi

function simplify { #Reduces a big bytes count down to megabytes or whatnot
    local steps num
    [ $1 ] || fdie "simplify() called without parameters\n  (requires a number of bytes with no unit name)"
    steps=0
    num=$1
    while [[ $(echo "$num > 1024" |bc) == 1 ]]
    do
        let steps++
        num=$(echo "$num/1024" |bc -l)
    done
    #Cut off after two decimal place:
    num=$(echo "$num" |sed 's/\(\.[0-9][0-9]\)[0-9]*$/\1/')
    printf "$num "
    case $steps in
        0)  echo b;;
        1)  echo KiB;;
        2)  echo MiB;;
        3)  echo GiB;;
        4)  echo TiB;;
        5)  echo PiB;;
        6)  echo EiB;;
        7)  echo ZiB;;
        8)  echo YiB;;
        *)  echo "1024 ^ $steps bytes";;
    esac
}

inst mountpoint || function mountpoint {
    local verbose dir rc
    verbose=false
    [[ $1 == -q ]] && shift                 #(always quiet! :)
    [[ $1 == -v ]] && verbose=true && shift
    dir="$1"
    $verbose && warn "dir=$dir"
    #if [[ $dir != /* ]]
    #then
    #    if [[ $PWD == / ]]
    #    then
    #        dir="/$dir"
    #    else
    #        dir="$PWD/$dir"
    #    fi
    #fi
    if [[ -d $dir ]]
    then
        pushd "$dir" &>/dev/null    || fdie "Couldn't cd to $dir"
        cd -P .      &>/dev/null    || ( warn "Couldn't perform cd -P ."; popd; return 1 )
        dir="$PWD"
        $verbose && warn "dir=$dir after processing"
        mount |grep -q " on $dir "; rc=$?
        $verbose && warn "mountpoint grep results: $(mount |grep -q ' on $dir ')"
    else
        $verbose && warn "$dir is not a directory"
        rc=1
    fi
    $verbose && warn "return code=$rc"
    popd         &>/dev/null
    return $rc
}

function diskmount {
    local device
    if [[ $1 ]]
    then
        device=${1#/dev}  #remove leading /dev/ if found
        udisksctl mount -b /dev/$device
    else
        fdie "usage: diskmount devname (i.e., sdb, not full path)"
    fi
}

function diskunmount {
    local device
    if [[ $1 ]]
    then
        device=${1#/dev}  #remove leading /dev/ if found
        udisksctl unmount -b /dev/$device && udisksctl power-off -b /dev/$device
    else
        fdie "usage: diskunmount devname (i.e., sdb, not full path)"
    fi
}

function pdfman {
    local output
    case $(uname) in
        OpenBSD) output=$(\man -Tpdf "$@")
                 [[ $output ]] && ( echo "$output" |zathura - --fork );;
        Linux)   output=$(\man -t "$@")
                 [[ $output ]] && ( echo "$output" |ps2pdf - - |zathura - --fork );;
        *)       fdie "I don't know how to \"$(uname)\""
    esac
}

function yt-mpv {
    local vertres targetres
    vertres=$(xrandr |grep " connected" |sed -E 's/^.* [0-9]{3,}x//; s/\+.*$//' |sort -n |tail -1)
    if (( $vertres >= 900 ))
    then
        targetres=1080
    else
        targetres=720
    fi
    mpv --ytdl --script-opts="ytdl_path=yt-dlp" --ytdl-raw-options=format="bestvideo[height<=$targetres][ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best" "$@"
}

function stconfig {     #Make sense of hideous syncthing XML config files
    local space1=25
    local space2=35
    printf "%-${space1}s%-${space2}s%s\n" "[Label]" "[Path]" "[Interval]"
    grep -oe 'label="[^"]*"' -e 'path="[^"]*"' -e 'rescanIntervalS="[^"]*"' \
        ~/.config/syncthing/config.xml |paste - - - \
        |awk -F '\t' '{printf "%-'${space1}'s%-'${space2}'s%s\n", $1, $2, $3}'
}

function aliasif {  #Shortcut for my .bash_aliases file, which creates an alias if the specified command is found
    [[ -n $1 && -n $2 ]] || return 1;
    if inst "$1"; then
        eval "alias $2";
    else
        return 2;
    fi
}

if [[ $(uname) == OpenBSD ]]; then
    function speakermute {
        mixerctl -q outputs.hp_source=dac-2:3
        mixerctl -q outputs.spkr_source=dac-0:1
        if [[ $(mixerctl -n inputs.dac-0:1) == 0,0 ]]; then
            #already muted, unmute
            mixerctl inputs.dac-0:1=64,64
        else
            #not muted, mute
            mixerctl inputs.dac-0:1=0,0
        fi
    }
fi

function restartplasma {    #Big, long sigh
    pgrep plasmashell &>/dev/null || fdie "Plasma shell not running."
    read -p "Are you sure you want to restart plasmashell? [y/N] > " ans
    if [[ $ans == [yY]* ]]; then
        printf "Stopping plasma..."
        kquitapp5 plasmashell
        printf " done. Return code was %d.\nWaiting 1 second..." $?
        sleep 1
        printf " done.\nStarting plasma...\n"
        nohup kstart5 plasmashell & sleep 1
        printf "Done. Return code was %d.\n" $?
    else
        echo "Aborted."
    fi
}

function invid {    #invidious
    local url useschemehandler=false
    local instance="https://"
    instance+="${INSTANCE:-inv.tux.pizza}"
    #instance+="${INSTANCE:-yewtu.be}"
    if [[ ${1:-} == -[bhg] ]]; then
        useschemehandler=true
        shift
    fi
    if [[ -n $1 ]]; then
        url="$instance/search?q=$(echo $* |tr [[:space:]] + |sed 's/+*$//')"
    else
        url="$instance/feed/subscriptions"
    fi
    url="${url%% }"
    if $useschemehandler; then
        xdg-open "$url" &>/dev/null & disown
    else
        w3m "$url"
    fi
}

#endend

true #(so that sourcing the file doesn't return a fail code)
